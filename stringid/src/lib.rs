//! The `stringid` crate is a set of helpers (macros, structs, traits) to manage identifiers
//! both as human readable string and unique number efficiently. It can be useful for using
//! immutable strings that exist until the end of the application.
//!
//! The unique identifier allows for light storage + fast comparison
//! and copy operations.
//! While the human readable string allows convenience use in UI, serialization, debug, etc.
//!
//! # Why use [`StringId`] ?
//! You can use [`StringId`] where you would like use an immutable [`String`] (or [`str`]) as an identifier.
//! And particularly if this identifier is used in several place in the data.
//! Two advantages of [`StringId`] are that :
//! - it does not duplicate the string data.
//! - it does not use string for comparison, assignation, copy of Id.
//!
//! ## ⚠ **Warning**
//! The use of `StringId` with a `StaticStrStore` can (safely) *leak memory*.
//! The memory allocated for the `str` will not be released until the end of the program, like a `'static` variable.
//! So you probably should avoid to use [`StringId`] with not immutable `String`,
//! if the identifiers (`String`/`str`) are likely to change regularly. Or do it knowingly.
//!
//! Typical uses can be :
//! - tags.
//! - inter-resources referencing, especially if you have to serialize/deserialize the data.
//!
//! # Technical design
//!
//! The [`StringId`] variables only store the identifier (a unique number).
//! While [`str`] are stored separately, only one time in memory for all copy of one Id
//! and are accessible through a dedicated type.
//!
//! The crate `stringid` components are designed to be used with [new-type pattern](https://doc.rust-lang.org/book/ch19-04-advanced-types.html#using-the-newtype-pattern-for-type-safety-and-abstraction).
//! So some macros are accessible trough the module `macros` to help define custom types.
//!
//! To store correspondence between a [`StringId`] and the corresponding [`str`] use an implementation of [`StrLookup`].
//! [`StrLookupHashMap`] implement [`StrLookup`] with an [`std::collections::HashMap`] and a [`StaticStrStore`].
//!
//! For more details, look at respective documentations.
//!
//! ## Quick start
//! ```
//! use std::{mem::size_of, str::FromStr};
//! use stringid::macros::{strlookup_hashmap, StringIdImpl};
//! use stringid::{create_hash_map, BufferStrStore, StringId};
//!
//! // Create a struct type `CustomIdLookup`
//! // as a `str` lookup with `BufferStrStore` as StrStore.
//! #[strlookup_hashmap(key = u32, store = BufferStrStore, size = 128)]
//! struct CustomIdLookup;
//!
//! // Create a `StringId` type who use the type `CustomIdLookup`
//! // as `StrLookup`.
//! #[derive(Debug, Clone, Copy, StringIdImpl)]
//! struct CustomId(StringId<u32, CustomIdLookup>);
//!
//! // Create instances of CustomId from str
//! let id_1 = CustomId::from_str("Id1").unwrap();
//! let id_2 = CustomId::from_str("Id2").unwrap();
//! let id_1_bis = id_1;
//! let id_1_ter = CustomId::from_str("Id1").unwrap();
//!
//! // CustomId size is the size of the Id (u32 here)
//! assert_eq!(size_of::<u32>(), size_of::<CustomId>());
//!
//! // The Id1 and Id2 are not equal
//! assert_ne!(id_1,id_2);
//! // But the id_1 and id_1_bis are
//! assert_eq!(id_1,id_1_bis);
//! // as well as id_1 and id_1_ter
//! assert_eq!(id_1,id_1_ter);
//! ```

mod static_str_buffer;
mod static_str_store;
mod str_lookup;
mod stringid;

pub use static_str_buffer::StaticStrBuffer;
pub use static_str_store::{BufferStrStore, StaticStrStore, StringStrStore};
pub use str_lookup::{create_hash_map, StrLookup, StrLookupEmpty, StrLookupHashMap};
pub use stringid::StringId;

#[cfg(feature = "macros")]
pub mod macros {
	pub use stringid_macros::strlookup_hashmap;
	pub use stringid_macros::StringIdImpl;
}
