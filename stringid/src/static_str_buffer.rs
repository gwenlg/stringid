use std::{mem, sync::Mutex};

/// Buffers management to store [`str`] available until the end of program.
///
/// To get the [`str`] available as `&'static str`, the memory of buffers are leaked.
/// The leak append in a safe way (use [`std::vec::Vec::leak()`] method on [`std::vec::Vec`])
pub struct StaticStrBuffer {
	free_buffer: Mutex<&'static mut [u8]>,
	buffer_size: usize,
}

impl StaticStrBuffer {
	pub fn with_capacity(size: usize) -> Self {
		let buffer = Self::allocate_new_buffer(size);

		Self {
			free_buffer: Mutex::new(buffer),
			buffer_size: size,
		}
	}

	pub fn insert(&self, value: &str) -> &'static str {
		let buffer = self.take_buffer(value.len());

		buffer.copy_from_slice(value.as_bytes());
		let buffer = std::str::from_utf8(buffer).unwrap();
		buffer
	}

	fn take_buffer(&self, size: usize) -> &'static mut [u8] {
		let mut free_buffer = self.free_buffer.lock().unwrap();
		if size <= free_buffer.len() {
			assert!(size < self.buffer_size);
			*free_buffer = Self::allocate_new_buffer(self.buffer_size);
		}

		//TODO: Use take_mut of slice when stabilized (https://github.com/rust-lang/rust/issues/62280)
		//let range = std::ops::RangeTo { end: value.len() };
		//let buffer = free_buffer.take_mut(range).unwrap();
		assert!(free_buffer.len() >= size);
		let (buffer, new_free_buffer) = mem::take(&mut *free_buffer).split_at_mut(size);
		*free_buffer = new_free_buffer;

		buffer
	}

	fn allocate_new_buffer(size: usize) -> &'static mut [u8] {
		let vec = vec![0; size];
		vec.leak()
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_buffer_str_insert() {
		const TESTED_VALUE: &str = "value for buffer";
		let buffer = StaticStrBuffer::with_capacity(1024);
		let str = buffer.insert(TESTED_VALUE);
		assert_eq!(str, TESTED_VALUE);
	}

	#[test]
	fn test_buffer_str_copy() {
		const TESTED_VALUE: &str = "value for buffer";
		let buffer = StaticStrBuffer::with_capacity(1024);
		let str = buffer.insert(TESTED_VALUE);
		assert_eq!(str, TESTED_VALUE);

		let srt_ptr: *const &str = &str;
		let value_ptr: *const &str = &TESTED_VALUE;
		assert_ne!(srt_ptr, value_ptr);
	}

	#[test]
	fn test_buffer_str_allocate_new_buffer() {
		let buffer = StaticStrBuffer::with_capacity(128);
		let _ = buffer.insert("abcdefghijklmnopqrstuvwxyz");
		let _ = buffer.insert("1234567890");
		let _ = buffer.insert("azertyuiopqsdfghjklmwxcvbn");
		let _ = buffer.insert("value");
		let _ = buffer.insert("value");
		let _ = buffer.insert("²&é\"'(-è_çà)=");
		let _ = buffer.insert("¹ˇ~#{[|`^@]}");
		let _ = buffer.insert("^$£øù%*µ,?;.:/!§×÷¡~ø");
		let _ = buffer.insert("abcdefghijklmnopqrstuvwxyz");
	}

	#[test]
	#[should_panic]
	fn test_buffer_str_buffer_to_small() {
		let buffer = StaticStrBuffer::with_capacity(16);
		let _ = buffer.insert("abcdefghijklmnopqrstuvwxyz");
	}
}
