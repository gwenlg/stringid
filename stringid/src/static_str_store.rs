use crate::static_str_buffer::StaticStrBuffer;
use once_cell::sync::Lazy;
use std::sync::Mutex;

/// A trait for store [`str`] until the end of program.
///
/// This allow to get `'static` lifetime on ref : `&'static str`
pub trait StaticStrStore {
	fn add(str: &str) -> &'static str;
}

/// A [`StaticStrStore`] where implementation leak allocated buffer of the [`String`]`.
///
/// This is a very simple implementation, but do one allocation by value.
#[derive(PartialEq)]
pub struct StringStrStore;

impl StaticStrStore for StringStrStore {
	fn add(value: &str) -> &'static str {
		String::from(value).leak()
	}
}

/// The size of buffer for [`BufferStrStore`]
const START_BUFFER_SIZE: usize = 128 * 1024; // use block of 128ko

/// A type than carry a [`StaticStrBuffer`] instance.
#[derive(PartialEq)]
pub struct BufferStrStore;

impl StaticStrStore for BufferStrStore {
	fn add(str: &str) -> &'static str {
		Self::buffer().lock().unwrap().insert(str)
	}
}

impl BufferStrStore {
	fn buffer() -> &'static Lazy<Mutex<StaticStrBuffer>> {
		static STR_BUFFER: Lazy<Mutex<StaticStrBuffer>> = Lazy::new(|| {
			println!("Initialisation of StaticStrBuffer");
			Mutex::new(StaticStrBuffer::with_capacity(START_BUFFER_SIZE))
		});
		&STR_BUFFER
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_string_str_store_1() {
		const DATA_1: &str = "data_1";
		let data_str = StringStrStore::add(DATA_1);
		// test than str content is equal to origin
		assert_eq!(data_str, DATA_1);
		// test than str content is copied (ptr are different)
		assert_ne!(data_str.as_ptr(), DATA_1.as_ptr());
	}

	#[test]
	fn test_string_str_store_2() {
		// test than str can be used out of the context creation
		const DATA_1: &str = "data_1";
		let static_str;
		{
			let data_str = StringStrStore::add(DATA_1);
			static_str = data_str;
		}
		assert_eq!(static_str, DATA_1);
		assert_ne!(static_str.as_ptr(), DATA_1.as_ptr());
	}

	#[test]
	fn test_buffer_str_store_1() {
		const DATA_1: &str = "data_1";
		let data_str = BufferStrStore::add(DATA_1);
		// test than str content is equal to origin
		assert_eq!(data_str, DATA_1);
		// test than str content is copied (ptr are different)
		assert_ne!(data_str.as_ptr(), DATA_1.as_ptr());
	}

	#[test]
	fn test_buffer_str_store_2() {
		// test than str can be used out of the context creation
		const DATA_1: &str = "data_1";
		let static_str;
		{
			let data_str = BufferStrStore::add(DATA_1);
			static_str = data_str;
		}
		assert_eq!(static_str, DATA_1);
		assert_ne!(static_str.as_ptr(), DATA_1.as_ptr());
	}
}
