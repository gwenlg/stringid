use crate::static_str_store::StaticStrStore;
use cityhasher::{FromCityHash, HashMap};
use once_cell::sync::Lazy;
use std::{fmt, hash::Hash, marker::PhantomData, sync::RwLock};

/// A trait for management of `str` associated with [`StringId`].
///
/// It should make link between a key (`Id` of a `StringId`) and an associated [`str`].
///
/// [`StringId`]: ../struct.StringId.html
pub trait StrLookup {
	type Key: fmt::Display;

	/// Insert the [`str`] value into lookup data, and return a corresponding unique key.
	fn insert(value: &str) -> Self::Key;

	/// Get the `str` corresponding do the key
	fn get_str(key: &Self::Key) -> &str;
}

/// An implementation of [`StrLookup`] witch don't keep the `str`.
///
/// The value associated with the key is lost.
/// Use it only when you don't need [`str`] anymore (like in retail data of a video game).
#[derive(PartialEq)]
pub struct StrLookupEmpty<LookupKey>
where
	LookupKey: fmt::Display,
{
	phantom: PhantomData<LookupKey>,
}

impl<LookupKey> StrLookup for StrLookupEmpty<LookupKey>
where
	LookupKey: fmt::Display + FromCityHash,
{
	type Key = LookupKey;

	fn insert(value: &str) -> Self::Key {
		let mut id_lowercase = String::from(value);
		id_lowercase.make_ascii_lowercase();
		cityhasher::hash(id_lowercase)
	}
	fn get_str(_key: &Self::Key) -> &str {
		const INVALID: &str = "<invalid>";
		INVALID
	}
}

/// A trait for management of `str` associated with [`StringId`] in a `'static` [`HashMap`].
///
/// The [`HashMap`] is managed by the type. This to be use directly in [`StringId`] types.
///
/// If you implement the trait [`StrLookupHashMap`] for a structure, the trait [`StrLookup`] is automatically implemented.
/// # Usage
/// Define a struct, implement the trait [`StrLookupHashMap`] for it
/// and use it in your [`StringId`] type(s).
///
/// [`StringId`]: struct.StringId.html
pub trait StrLookupHashMap {
	type Key: fmt::Display;
	type StrStore: StaticStrStore;

	/// provide access to a `'static` [`HashMap`].
	fn hashmap() -> &'static RwLock<HashMap<Self::Key, &'static str>>;
}

impl<KeyType, StoreType, U> StrLookup for U
where
	KeyType: fmt::Display + Eq + PartialEq + Hash + FromCityHash + Clone + Copy + 'static,
	StoreType: StaticStrStore,
	U: StrLookupHashMap<Key = KeyType, StrStore = StoreType>,
{
	type Key = KeyType;

	fn insert(value: &str) -> Self::Key {
		let mut id_lowercase = String::from(value);
		id_lowercase.make_ascii_lowercase();
		let key = cityhasher::hash(&id_lowercase);

		let prev_value = Self::hashmap().read().unwrap().get(&key).copied();
		if let Some(prev_value) = prev_value {
			// alread present, nothing to do.
			// But check if it's indeed the same value.
			assert_eq!(
				{
					let mut prev_value = String::from(prev_value);
					prev_value.make_ascii_lowercase();
					prev_value
				},
				id_lowercase
			);
		} else {
			let value = U::StrStore::add(value);
			let prev_value = Self::hashmap().write().unwrap().insert(key, value);
			// Don't unwrap on prev_value == None, in multithreaded environment, an insert can happen between
			// the read + get, and the write + insert
			if let Some(prev_value) = prev_value {
				assert!(prev_value == id_lowercase);
			}
		}
		key
	}
	fn get_str(key: &Self::Key) -> &str {
		let hashmap = Self::hashmap().read().unwrap();
		hashmap.get(key).unwrap()
	}
}

/// Create a [`HashMap`] protected by a [`RwLock`].
///
/// It configure [`HashMap`] to use [`cityhasher`] hasher.
///
/// This is use by macro `strlookup_hashmap` who implement [`StrLookupHashMap`].
/// # Generics
/// * `T` represent the type of the [`HashMap`] key.
pub fn create_hash_map<T>(
	table_size: usize,
) -> std::sync::RwLock<std::collections::HashMap<T, &'static str, cityhasher::CityHasher>> {
	let s = cityhasher::CityHasher::new();
	let hash_map = cityhasher::HashMap::<T, &'static str>::with_capacity_and_hasher(table_size, s);
	std::sync::RwLock::new(hash_map)
}

#[derive(PartialEq)]
pub struct StrLookupHashMap32<Store> {
	phantom_store: PhantomData<Store>,
}

impl<Store> StrLookupHashMap for StrLookupHashMap32<Store>
where
	Store: StaticStrStore,
{
	type Key = u32;
	type StrStore = Store;

	fn hashmap() -> &'static RwLock<HashMap<Self::Key, &'static str>> {
		static STRING_TABLE: Lazy<RwLock<HashMap<u32, &'static str>>> =
			Lazy::new(|| create_hash_map::<u32>(100));
		&STRING_TABLE
	}
}

#[derive(PartialEq)]
pub struct StrLookupHashMap64<Store> {
	phantom: PhantomData<Store>,
}

impl<Store> StrLookupHashMap for StrLookupHashMap64<Store>
where
	Store: StaticStrStore,
{
	type Key = u64;
	type StrStore = Store;

	fn hashmap() -> &'static RwLock<HashMap<u64, &'static str>> {
		static STRING_TABLE: Lazy<RwLock<HashMap<u64, &'static str>>> =
			Lazy::new(|| create_hash_map::<u64>(100));
		&STRING_TABLE
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::static_str_store::{BufferStrStore, StringStrStore};

	#[test]
	fn test_lookup_empty() {
		let key = StrLookupEmpty::<u32>::insert("data");
		let str = StrLookupEmpty::<u32>::get_str(&key);
		assert_eq!(str, "<invalid>");
	}

	#[test]
	fn test_string_str_store_insert_and_get() {
		const WALK_STR: &str = "walk";
		const RUN_STR: &str = "run";

		let walk_key = StrLookupHashMap32::<StringStrStore>::insert(WALK_STR);
		let walk_str = StrLookupHashMap32::<StringStrStore>::get_str(&walk_key);
		assert_eq!(walk_str, WALK_STR);

		let run_key = StrLookupHashMap64::<StringStrStore>::insert(RUN_STR);
		let run_str = StrLookupHashMap64::<StringStrStore>::get_str(&run_key);
		assert_eq!(run_str, RUN_STR);
	}

	#[test]
	fn test_buffer_str_store_insert_and_get() {
		const SMILE_STR: &str = "smile";
		const GREET_STR: &str = "greet";

		let smile_key = StrLookupHashMap32::<BufferStrStore>::insert(SMILE_STR);
		let smile_str = StrLookupHashMap32::<BufferStrStore>::get_str(&smile_key);
		assert_eq!(smile_str, SMILE_STR);

		let greet_key = StrLookupHashMap64::<BufferStrStore>::insert(GREET_STR);
		let greet_str = StrLookupHashMap64::<BufferStrStore>::get_str(&greet_key);
		assert_eq!(greet_str, GREET_STR);
	}
}
