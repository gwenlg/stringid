use std::{mem::size_of, str::FromStr};

use stringid::{
	create_hash_map,
	macros::{strlookup_hashmap, StringIdImpl},
	BufferStrStore, StringId,
};

#[strlookup_hashmap(key = u32, store = BufferStrStore, size = 128)]
struct AnimNameLookup;

#[derive(Clone, Copy, Debug, StringIdImpl)]
struct AnimName(StringId<u32, AnimNameLookup>);

fn main() {
	assert_eq!(size_of::<u32>(), size_of::<AnimName>());

	let anim_name_walk = AnimName::from_str("Walk").unwrap();
	let anim_name_run = AnimName::from_str("Run").unwrap();
	let anim_name_walk_copy = anim_name_walk;

	assert_eq!(anim_name_walk, anim_name_walk_copy);
	assert_ne!(anim_name_walk, anim_name_run);

	let anim_name_run_bis = AnimName::from_str("Run").unwrap();
	assert_eq!(anim_name_run, anim_name_run_bis);

	println!("Anim '{anim_name_walk}' : {anim_name_walk:?}");
	println!("Anim '{anim_name_run}' : {anim_name_run:?}");
	println!("Size_of : {}", std::mem::size_of::<AnimName>());

	println!("{anim_name_walk} {} {anim_name_run}", {
		if anim_name_walk == anim_name_run {
			"=="
		} else {
			"!="
		}
	});

	let mut anims = vec![anim_name_walk, anim_name_run];
	anims.sort();
	println!("{anims:?}");
}
