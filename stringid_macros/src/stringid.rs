use proc_macro2::TokenStream;
use quote::quote;
use syn::{DeriveInput, Ident, Result, TypePath};

use crate::parse::*;

pub(crate) fn get_stringid_type(input: &DeriveInput) -> Result<&TypePath> {
	let data_struct = get_data_struct(input)?;
	let field = get_unnamed_field(data_struct)?;
	assert!(field.ident.is_none());
	let path = get_field_path(field)?;
	Ok(path)
}

pub(crate) fn get_stringid_impl(stringid_type: TokenStream, name: Ident) -> TokenStream {
	quote! {
		impl From<#stringid_type> for #name {
			fn from(item: #stringid_type) -> Self {
				Self(item)
			}
		}
		impl std::str::FromStr for #name {
			type Err = ();

			fn from_str(s: &str) -> Result<Self, ()> {
				let id_str = #stringid_type::from_str(s);
				match id_str {
					Ok(id_str) => Ok(Self(id_str)),
					Err(err) => Err(err),
				}
			}
		}
		impl std::fmt::Display for #name {
			fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
				self.0.fmt(f)
			}
		}
		impl std::cmp::PartialEq for #name {
			fn eq(&self, other: &Self) -> bool {
				self.0.id().eq(&other.0.id())
			}
		}
		impl std::cmp::Eq for #name{}
		impl std::cmp::PartialOrd for #name {
			fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
				Some(self.cmp(other))
			}
		}
		impl std::cmp::Ord for #name {
			fn cmp(&self, other: &Self) -> std::cmp::Ordering {
				self.0.get_str().cmp(&other.0.get_str())
			}
		}
	}
}
