//! Macros for `stringid` crate. Look at her doc for more information.
mod generate;
mod parse;
mod stringid;
mod strlookup_hashmap;

use proc_macro::TokenStream;
use strlookup_hashmap::get_strlookup_hashmap_impl;
use syn::{parse_macro_input, DeriveInput};

use crate::generate::to_tokenstream;
use crate::stringid::{get_stringid_impl, get_stringid_type};
use crate::strlookup_hashmap::{get_strlookup_type, StrLookupHashMap};

/// Implement useful traits to new-type based on [`StringId`].
///
/// It implement the trait `From<StringId<Id>>`, and other
/// like : [`std::str::FromStr`], [`std::fmt::Display`], [`std::cmp::PartialEq`],
///  [`std::cmp::Eq`], [`std::cmp::PartialOrd`], [`std::cmp::Ord`]
///
/// [`StringId`]: ../struct.StringId.html
#[proc_macro_derive(StringIdImpl)]
pub fn stringid_macro_derive(input: TokenStream) -> TokenStream {
	let input = parse_macro_input!(input as DeriveInput);

	let name = input.ident.clone();
	match get_stringid_type(&input) {
		Ok(path) => {
			let stringid_type = to_tokenstream(path);
			let expanded = get_stringid_impl(stringid_type, name);

			TokenStream::from(expanded)
		}
		Err(err) => err.into_compile_error().into(),
	}
}

/// Setup an empty struct as [`StrLookupHashMap`].
///
/// You should indicate :
/// - key: the type of the key ([`u32`] or [`u64`]). It's should correspond to the `Id` type of the [`StringId`].
/// - store: the type of the [`StaticStrStore`].
/// - size: the size of the [`std::collections::HashMap`]
/// # Example:
/// ```ignore
/// #[strlookup_hashmap(key = u32, store = BufferStrStore, size = 128)]
/// struct CustomIdHashMap;
/// ```
///
/// [`StringId`]: ../struct.StringId.html
/// [`StaticStrStore`]: ../trait.StaticStrStore.html
/// [`StrLookupHashMap`]: ../trait.StrLookupHashMap.html
#[proc_macro_attribute]
pub fn strlookup_hashmap(attrs: TokenStream, item: TokenStream) -> TokenStream {
	let input = parse_macro_input!(item as DeriveInput);
	let name = input.ident.clone();

	match get_strlookup_type(&input) {
		Ok(()) => {}
		Err(err) => return err.into_compile_error().into(),
	}

	let attrs = syn::parse::<StrLookupHashMap>(attrs);
	match attrs {
		Ok(strlookup) => {
			let expanded = get_strlookup_hashmap_impl(
				&name,
				strlookup.key_type,
				strlookup.store_type,
				strlookup.hashmap_size,
			);

			TokenStream::from(expanded)
		}
		Err(err) => err.into_compile_error().into(),
	}
}
