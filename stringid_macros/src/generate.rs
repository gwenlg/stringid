use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use syn::PathArguments;

pub(crate) fn to_tokenstream(path: &syn::TypePath) -> TokenStream {
	match path.path.get_ident() {
		Some(ident) => ident.into_token_stream(),
		None => {
			let segment = path.path.segments.iter().next().unwrap();
			let root = &segment.ident;
			let stream = match &segment.arguments {
				PathArguments::AngleBracketed(args) => args,
				_ => panic!("PathArguments other than AngleBracketed are not management"),
			};

			quote! {#root :: #stream}
		}
	}
}
